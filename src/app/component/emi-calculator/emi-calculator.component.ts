import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-emi-calculator',
  templateUrl: './emi-calculator.component.html',
  styleUrls: ['./emi-calculator.component.scss']
})
export class EmiCalculatorComponent implements OnInit {
  emi: number = 0;
  totalInterest: number = 0;
  totalPayment: number = 0;
  tableData: any[] = [];
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  EMICalculationForm: FormGroup = this.fb.group({
    loanAmount: ['', [Validators.required]],
    interestRate: ['', [Validators.required]],
    loanTenure: ['', [Validators.required]]
  });

  calculateEMI() {
    let loanAmount = this.EMICalculationForm?.get('loanAmount')?.value;
    let rate = this.EMICalculationForm?.get('interestRate')?.value/1200;
    let tenure = (this.EMICalculationForm?.get('loanTenure')?.value)*12;
    this.emi = (loanAmount * rate * Math.pow(1+rate, tenure)) / (Math.pow(1+rate, tenure) - 1);
    
    this.totalPayment = Math.round(this.emi * tenure);
    this.totalInterest = Math.round(this.totalPayment - loanAmount);
    this.generateTableData();
  }

 generateTableData() {
    let r = this.EMICalculationForm?.get('interestRate')?.value / (12 * 100);
    let n = this.EMICalculationForm?.get('loanTenure')?.value * 12;
    let balance = this.EMICalculationForm?.get('loanAmount')?.value;
    let paidToDate = 0;
    for (let i = 0; i < n; i++) {
      let interest = Math.round(balance * r);
      let principal = Math.round(this.emi - interest);
      balance -= principal;
      paidToDate += principal;
      let year = Math.floor((new Date().getFullYear() * 12 + i) / 12);
      let month = (new Date().getFullYear() * 12 + i) % 12 + 1;
      let date = this.getMonthName(month) + ' - ' + year;
      let rowData = {
        date,
        principal,
        interest,
        total: principal + interest,
        balance,
        paidToDate,
        rate: (paidToDate / this.EMICalculationForm?.get('loanAmount')?.value * 100).toFixed(2) + '%'
      };
      this.tableData.push(rowData);
    }
  }

  getMonthName(month: number) {
    let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    return months[month - 1];
  }
}
